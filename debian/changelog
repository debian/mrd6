mrd6 (0.9.6-14) unstable; urgency=medium

  [ Janitor bot ]
  * debian/changelog
    + Trim whitespace on empty line.
    + Fix day of week in old entries.
  * Build-depend on debhelper-compat to get debhelper and express compat level.
  * Use canonical URL in Vcs-Browser.

  [ Thomas Preud'homme ]
  * debian/control:
    + Update Vcs fields.
    + Bump compat level to 13.
    + Bump standard version to 4.5.0.
    + do not require root for building.
  * debian/rules:
    + Enable all hardening.
    + Use execute_before/execute_after to simplify file.
  * debian/upstream/metadata:
    + Document information about upstream project.
  * debian/patches:
    + Fix typo in patch to fix build warnings.
  * Add systemd service unit.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces

 -- Thomas Preud'homme <robotux@debian.org>  Mon, 14 Sep 2020 23:34:22 +0100

mrd6 (0.9.6-13) unstable; urgency=medium

  * Make build reproducible (Closes: #829765, thanks Reiner Herrmann).
  * Bump Standards-Version to 3.9.8 (no changes needed).
  * debian/watch: use direct link to tags page rather than deprecated
    githubredir.
  * debian/control: use secure URL for Vcs-git.

 -- Thomas Preud'homme <robotux@debian.org>  Thu, 01 Sep 2016 19:35:05 +0100

mrd6 (0.9.6-12) unstable; urgency=medium

  * Remove dependency on initscripts (Closes: #804959).
  * Make Vcs-* field canonical.
  * Remove DMUA field.
  * Bump Standards-Version to 3.9.6 (no changes needed).

 -- Thomas Preud'homme <robotux@debian.org>  Sat, 14 Nov 2015 20:27:02 +0800

mrd6 (0.9.6-11) unstable; urgency=low

  * debian/control:
    + Update maintainer email address.
  * debian/copyright:
    + Update maintainer email address.

 -- Thomas Preud'homme <robotux@celest.fr>  Sun, 26 Aug 2012 17:32:06 +0200

mrd6 (0.9.6-10) unstable; urgency=low

  * Make sure new interface are correctly marked up by mrd6 (Closes: #674738).

 -- Thomas Preud'homme <robotux@celest.fr>  Wed, 13 Jun 2012 13:14:11 +0200

mrd6 (0.9.6-9) unstable; urgency=low

  * Improve fortification of mrd6 (Closes: #662191, big thanks Simon
    Ruderich):
    + Enable verbose build to catch missing flags.
    + Always allow flags to be set from environment.
    + Use LDFLAGS when linking modules.
    + Interpret syslog buffer string as string only.
    + Remove manual setting of *FLAGS with dpkg-buildflags in debian/rules.

 -- Thomas Preud'homme <robotux@celest.fr>  Sun, 04 Mar 2012 21:48:42 +0100

mrd6 (0.9.6-8) unstable; urgency=low

  * Fix support of s390x: s390x is now excluded from tests for s390.

 -- Thomas Preud'homme <robotux@celest.fr>  Sun, 04 Mar 2012 12:19:21 +0100

mrd6 (0.9.6-7) unstable; urgency=low

  * Store PID file in the new /run directory.
  * debian/control:
    + Add support for s390x.
    + Remove Hurd from valid architectures as some code is missing to build
      correctly for now.
    + Set DMUA flag.
    + Bump Standards-Version to 3.9.3 (no changes needed).
  * debian/rules:
    + Use dpkg-buildflags to set build flags (CPPFLAGS, CXXFLAGS and LDFLAGS).
  * debian/copyright:
    + Use final version for DEP5 1.0 copyright.
    + Document global license and copyright.
    + Add missing dots between paragraph in Expat license statement.
    + Add missing hyphens in GPL-2+ license shortname.
    + Move GPL-2+ to a separate license stanza.
  * Update debian/compat to 9 (no changes needed).

 -- Thomas Preud'homme <robotux@celest.fr>  Sat, 03 Mar 2012 00:41:22 +0100

mrd6 (0.9.6-6) unstable; urgency=low

  * Fix errors and warnings from last build on amd64:
    + Fix FTBFS on amd64 due to missing cstddef include (Closes: #625100).
    + Uses %zi for displaying the content of a size_t variable.
    + Remove unaccessed prevpointer in parser.cpp.
  * Use the same name as upstream for init script to avoid mistakes.
  * Fix a code typo in patch fixing Hurd FTBFS.
  * Remove duplicate clean target in debian/rules.

 -- Thomas Preud'homme <robotux@celest.fr>  Sat, 23 Apr 2011 19:11:24 +0200

mrd6 (0.9.6-5) unstable; urgency=low

  * Don't use any debian/* files from orig.tar.gz.

 -- Thomas Preud'homme <robotux@celest.fr>  Fri, 22 Apr 2011 00:27:32 +0200

mrd6 (0.9.6-4) unstable; urgency=low

  * Fix FTBFS on Hurd.
  * Add missing dependency on $syslog in init script.
  * debian/watch:
    + Track new upstream repository on github.
  * debian/control:
    + Recommends perl as mrd6sh needs it.
    + Bumps Standards-Version to 3.9.2 (no changes needed).
  * debian/copyright:
    + Convert to DEP5 candidate format.
  * debian/compat:
    + Bumps debhelper compatibility from 7 to 8 (no changes needed).

 -- Thomas Preud'homme <robotux@celest.fr>  Mon, 18 Oct 2010 22:01:51 +0200

mrd6 (0.9.6-3) unstable; urgency=low

  * Make dl* functions available on all archs (Fix FTBFS on *BSD).
  * debian/control:
    + Bumps Standards-Version to 3.9.1 (no changes needed).

 -- Thomas Preud'homme <robotux@celest.fr>  Mon, 09 Aug 2010 22:18:03 +0200

mrd6 (0.9.6-2) unstable; urgency=low

  * New maintainer. (Closes: #572373)
  * Acknowledge NMUs. (Closes: #507211)
  * Make Makefile architecture aware (fix FTBFS on *BSD).
  * Fix FTBFS on Hurd.
  * Support status command in init script.
  * Fix lintian errors and warnings:
    - Add dependency on remote_fs in init script
    - Use invoke-rc.d in init script
    - Add a ${misc:Depends} build dependency.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Update Standards-Version to 3.8.4.
  * Bumps debhelper compatibility to 7.
    - Uses a dh based debian/rules.
  * Fix build warnings:
    - Disable strict aliasing as mrd6 violates aliasing rules
    - Avoid unnecessary libgcc dependency thanks to Pierre Habouzit
  * Deactivate watch file as upstream tarballs are no longer reachable.
  * Set Vcs-Browser and Vcs-Git fields.

 -- Thomas Preud'homme <robotux@celest.fr>  Wed, 05 May 2010 22:47:16 +0200

mrd6 (0.9.6-1.3) unstable; urgency=low

  * Non-maintainer upload.
  * add #include's in src/mrd.cpp and src/bgp/bgp.cpp to fix FTBFS.
    Thanks, James Westby for the patch.
    (Closes: #507211)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 22 Mar 2009 20:13:42 -0400

mrd6 (0.9.6-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Apply patch from upstream (r1722) to fix FTBFS on s390. Affects:
    - include/mrd/log.h
    - src/log.cpp

 -- Laurent Bigonville <bigon@debian.org>  Tue, 16 Sep 2008 19:53:14 +0200

mrd6 (0.9.6-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Apply patch from upstream to fix FBTFS on s390, mips and mipsel. Effects:
    - src/linux/mrd_components.cpp
    - src/mrd.cpp

 -- Lior Kaplan <kaplan@debian.org>  Sat, 09 Feb 2008 13:08:17 +0200

mrd6 (0.9.6-1) unstable; urgency=low

  [ Hugo Santos ]
  * New upstream release (Closes: #462786)
    - Incorporates NMU changes (Closes: #423063, #417436)
  * Now really fix FTBFS with GCC 4.3 (Closes: #455280)

  [ Lior Kaplan ]
  * Bump standards-version to 3.7.3
    - Move homepage to a regular header.
  * Fix debian-changelog-line-too-long lintian warning.
  * Fix debian-rules-ignores-make-clean-error lintain warning.
  * Add a debian version number to differentiate this package from upstream's
    native package.

 -- Hugo Santos <hugo@fivebits.net>  Thu, 31 Jan 2008 11:51:25 +0200

mrd6 (0.9.5-rev3.dfsg-0.2) unstable; urgency=low

  * Non-maintainer upload.
  * Repackage upstream tarball to remove non-free IETF RFC/I-D (Closes: #423063)
  * Really fix FTBFS with GCC 4.3 (Closes: #417436)
  * Clean up debian/rules and create debian/{examples,manpages}

 -- Lior Kaplan <kaplan@debian.org>  Sat, 09 Jun 2007 00:57:01 +0300

mrd6 (0.9.5-rev3-0.1) unstable; urgency=low

  * Non-maintainer upload. (Closes: #418074)
  * New maintainer. (Closes: #418084)
    - Adopt this package by its upstream, which works on it very tightly with
      Debian. Thank you Hugo for covering for an MIA maintainer.
  * New upstream release
    - Fix segfault when unloading module (Closes: #394590)
    - Fix FTBFS with GCC 4.3 (Closes: #417436)
    - New upstream address (Closes: #411805)
    - Change the package's version number inside debian, so it will be more
      accurate and easy to maintain with future (debian & upstream) versions.
  * debian/control:
    - Bump standards version to 3.7.2.2 (no changes needed)
    - Remove extra build depends, as they are covered by build-essential.
    - Update package description according to the README file.
    - Change maintainer to Hugo Santos.
  * Add watch file.

 -- Lior Kaplan <kaplan@debian.org>  Mon, 09 Apr 2007 20:28:32 +0300

mrd6 (0.9.5-release-1) unstable; urgency=low

  * New upstream release
  * Hack the version number so we can upgrade properly
  * New features:
        - update to draft 06
        - implements MSNIP and MRDISC
        - improved user-space forwarding on Linux

 -- Anand Kumria <wildfire@progsoc.org>  Sun,  8 Jan 2006 12:28:48 +1100

mrd6 (0.9.5-pre2-1) unstable; urgency=low

  * New upstream (pre)release

 -- Anand Kumria <wildfire@progsoc.org>  Mon, 28 Nov 2005 15:41:11 +1100

mrd6 (0.9.5-pre1-1) unstable; urgency=low

  * Initial upload to Debian

 -- Anand Kumria <wildfire@progsoc.org>  Tue, 15 Nov 2005 11:05:57 +1100

mrd6 (0.9.4-beta2-1) unstable; urgency=low

  * beta2

 -- Hugo Santos <hsantos@av.it.pt>  Mon, 17 Oct 2005 22:57:45 +0100

mrd6 (0.9.4-beta1-1) unstable; urgency=low

  * Bumped to 0.9.4-beta1

 -- Hugo Santos <hsantos@av.it.pt>  Wed, 28 Sep 2005 01:11:03 +0100

mrd6 (0.9.2-beta3-1) unstable; urgency=low

  * beta3

 -- Hugo Santos <hsantos@av.it.pt>  Tue, 23 Aug 2005 23:36:40 +0100

mrd6 (0.9.2-beta2-1) unstable; urgency=low

  * beta2

 -- Hugo Santos <hsantos@av.it.pt>  Thu, 18 Aug 2005 00:09:07 +0100

mrd6 (0.9.2-beta1-1) unstable; urgency=low

  * Bumped to 0.9.2.

 -- Hugo Santos <hsantos@av.it.pt>  Fri, 12 Aug 2005 00:30:29 +0100

mrd6 (0.9.1-beta2-1) unstable; urgency=low

  * Version bump

 -- Hugo Santos <hsantos@av.it.pt>  Mon, 25 Jul 2005 00:32:34 +0100

mrd6 (0.9.1-beta1-1) unstable; urgency=low

  * Version bump

 -- Hugo Santos <hsantos@av.it.pt>  Thu,  7 Jul 2005 02:34:26 +0100

mrd6 (0.9-beta2) unstable; urgency=low

  * Beta 2

 -- Hugo Santos <hsantos@av.it.pt>  Thu, 30 Jun 2005 06:19:51 +0100

mrd6 (0.9-beta1) unstable; urgency=low

  * Bump to 0.9

 -- Hugo Santos <hsantos@av.it.pt>  Sat, 25 Jun 2005 20:07:24 +0100

mrd6 (0.8.5-beta1-1) unstable; urgency=low

  * Bumped to 0.8.5

 -- Hugo Santos <hsantos@av.it.pt>  Sat, 30 Apr 2005 21:55:09 +0100

mrd6 (0.8-beta2-1) unstable; urgency=low

  * Yet another bump. 0.8-beta2

 -- Hugo Santos <hsantos@av.it.pt>  Sat, 23 Apr 2005 03:06:53 +0100

mrd6 (0.8-beta1-1) unstable; urgency=low

  * Bumped version to 0.8-beta1

 -- Hugo Santos <hsantos@av.it.pt>  Tue, 19 Apr 2005 23:34:04 +0100

mrd6 (0.7-beta3-1) unstable; urgency=low

  * Bumped version to 0.7-beta3

 -- Hugo Santos <hsantos@av.it.pt>  Tue,  5 Apr 2005 02:08:32 +0100

mrd6 (0.7-beta2-1) unstable; urgency=low

  * Bumped version to 0.7-beta2

 -- Hugo Santos <hsantos@av.it.pt>  Sat, 26 Mar 2005 03:46:26 +0000

mrd6 (0.7-beta1-1) unstable; urgency=low

  * Bumped version to 0.7-beta1

 -- Hugo Santos <hsantos@av.it.pt>  Thu, 24 Mar 2005 14:52:00 +0000

mrd6 (0.6-beta6-1) unstable; urgency=low

  * Bumped version to beta6

 -- Hugo Santos <hsantos@av.it.pt>  Mon, 07 Mar 2005 23:13:08 +0000

mrd6 (0.6-beta5-1) unstable; urgency=low

  * Initial Release.

 -- Hugo Santos <hsantos@av.it.pt>  Sun,  27 Feb 2005 03:08:08 +0000
