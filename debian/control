Source: mrd6
Section: net
Priority: optional
Maintainer: Thomas Preud'homme <robotux@debian.org>
Build-Depends: debhelper-compat (= 13), perl
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: http://fivebits.net/proj/mrd6/
Vcs-Browser: https://salsa.debian.org/debian/mrd6
Vcs-Git: https://salsa.debian.org/debian/mrd6.git

Package: mrd6
Architecture: linux-any kfreebsd-any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, lsb-base
Recommends: ${perl:Depends}
Description: IPv6 Multicast Routing Daemon
 mrd6 is a modular IPv6 Multicast Routing Daemon which implements:
   * MLDv1 and MLDv2 support
     - MLD proxying
   * PIM-SM (ASM and SSM)
     - Bootstrap (BSR) Mechanism support
     - Static RP configuration
     - Embedded-RP support (RFC 3956)
   * partial MBGP support
     - Implements basic BGP4 mechanisms (IPv6 Multicast SAFI)
       to update local MRIB from MBGP info
     - Uses IPv6 Multicast SAFI prefixes announced by
       peers to update local MRIB
     - Is able to announce local prefixes
     - Filter support
   * Supports both native and virtual (tunnel) interfaces
     (tested IPv6-IPv4, IPv6-IPv6 and TUN/TAP tunnels)
   * Abstract Forwarding Interface (MFA) with user-space module
   * Remote configuration and online administration
